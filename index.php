<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 19:16
 */

require_once "vendor/autoload.php";

$token = 'you_api_key';
date_default_timezone_set('UTC');

$validCards = 'take->Train|from->Madrid|to->Barselona|departs->2017-05-26T13:00:00+0000|arrives->2017-05-26T12:15:00+0000|board->78A|seat->45B|baggage->null;';
$validCards .= 'take->Flight|from->Stockholm|to->New York JFK|departs->2017-05-26T20:00:00+0000|arrives->2017-05-26T23:00:00+0000|gate->22|board->SK22|seat->7B|baggage->will we automatically transferred from your last leg;';
$validCards .= 'take->Bus|from->Barcelona|to->Gerona Airport|departs->2017-05-26T13:00:00+0000|arrives->2017-05-24T15:00:00+0000;';
$validCards .= 'take->Flight|from->Gerona Airport|to->Stockholm|departs->2017-05-26T16:00:00+0000|arrives->2017-05-26T18:00:00+0000|gate->45B|board->SK455|seat->3A|baggage->drop at ticket counter 344';



$api = new \Dblanko\Travel\TravelClass($token);
$result = $api->call('buildResult', $validCards);

if ($result instanceof \Dblanko\Travel\Calc) {

    foreach ($result->cards as $i=>$card) {
        echo \Dblanko\Travel\TravelClass::parseOutput($card);
        if (!empty($result->pathConsistencyWarnings[$i])) {
            echo 'UNCONSISTENT PATH' . $result->pathConsistencyWarnings[$i];
        }

        if (!empty($result->timeConsistencyWarnings[$i])) {
            echo 'UNCONSISTENT PATH' . $result->timeConsistencyWarnings[$i];
        }

        if (!empty($result->timeNotice[$i])) {
            echo 'Very low time limit' . $result->timeNotice[$i];
        }
    }
}


