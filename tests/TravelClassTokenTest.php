<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:03
 */

namespace Dblanko\Travel\Test;

use Dblanko\Travel\TravelClass as Travel;

/**
 * @runTestsInSeparateProcesses
 */
class TravelClassTokenTest extends \PHPUnit_Framework_TestCase
{
    public function invalidTokens()
    {
        return [
            'empty'    	=> [ '' ],
            'short'        	=> [ 'short' ],
            'digit'    	=> [ 1 ],
            'multi-digit' => [ 123 ],
            'bool'     	=> [ true ],
            'array'    	=> [ ['token'] ],
        ];
    }

    public function validTokens()
    {
        return [
            'token'  	=> [ 'token6' ],
            'short-hash' => [ '123456789' ],
            'full-hash'  => [ 'akrwejhtn983z420qrzc8397r4' ],
        ];
    }

    /**
     * @dataProvider invalidTokens
     */
    public function testSetTokenRaisesExceptionOnInvalidToken($token)
    {
        $this->setExpectedException('InvalidArgumentException');
        Travel::setToken($token);
    }

    /**
     * @dataProvider validTokens
     */
    public function testSetTokenSucceedsOnValidToken($token)
    {
        Travel::setToken($token);
        $api = new Travel();
        $this->assertInstanceOf('\Dblanko\Travel\TravelClass', $api);
    }

}
