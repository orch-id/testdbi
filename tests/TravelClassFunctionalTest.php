<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:07
 */

namespace Dblanko\Travel\Test;
require_once "vendor/autoload.php";

use Dblanko\Travel\Calc;
use Dblanko\Travel\Exceptions\TravelException;
use Dblanko\Travel\TravelClass as Travel;

date_default_timezone_set('UTC');
$token = 'you_api_key';
$testHR = PHP_EOL . '_______________________________________' . PHP_EOL. PHP_EOL;

$validCards = '[
    {
        "from":"Madrid","to":"Barcelona","departs":"2017-05-26T00:05:00+0000","arrives":"2017-05-26T12:15:00+0000","type":"Train","board":"78A","seat":"45B","baggage":null
    },{
        "from":"Stockholm","to":"New York JFK","departs":"2017-05-26T20:00:00+0000","arrives":"2017-05-26T23:00:00+0000","type":"Flight","gate":"22","board":"SK22","seat":"7B","baggage":"will we automatically transferred from your last leg"
    },{   
        "from":"Barcelona","to":"Gerona Airport","departs":"2017-05-26T13:00:00+0000","arrives":"2017-05-26T15:00:00+0000","type":"Bus","board":null,"seat":null,"baggage":null
    },{
        "from":"Gerona Airport","to":"Stockholm","departs":"2017-05-26T16:00:00+0000","arrives":"2017-05-26T18:00:00+0000","type":"Flight","gate":"45B","board":"SK455","seat":"3A","baggage":"drop at ticket counter 344"
    }
]';







echo $testHR;




// test empty cards
$api = new Travel($token);
$emptyCards = '';
try {
    $result = $api->call('buildResult', $emptyCards);
    if ($result) {
        throw new \Exception('Should be TravelException empty cards list');
    }
} catch (TravelException $e) {
    echo 'Empty Cards test passed: TravelException ' . $e->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    throw new \Exception('Something went wrong ' . $e->getMessage() . PHP_EOL);
}
echo $testHR;




// test $unConsistTimeCards
$api = new Travel($token);
$unConsistTimeCards = 'take->Train|from->Madrid|to->Barselona|departs->2017-05-26T13:00:00+0000|arrives->2017-05-26T12:15:00+0000|board->78A|seat->45B|baggage->null;';
$unConsistTimeCards .= 'take->Flight|from->Stockholm|to->New York JFK|departs->2017-05-26T20:00:00+0000|arrives->2017-05-26T23:00:00+0000|gate->22|board->SK22|seat->7B|baggage->will we automatically transferred from your last leg;';
$unConsistTimeCards .= 'take->Bus|from->Barcelona|to->Gerona Airport|departs->2017-05-24T13:00:00+0000|arrives->2017-05-24T15:00:00+0000;';
$unConsistTimeCards .= 'take->Flight|from->Gerona Airport|to->Stockholm|departs->2017-05-26T16:00:00+0000|arrives->2017-05-26T18:00:00+0000|gate->45B|board->SK455|seat->3A|baggage->drop at ticket counter 344';

try {
    $result = $api->call('buildResult', $unConsistTimeCards);
    if ($result instanceof Calc) {

        if (!$result->timeConsistencyWarnings) {
            print ('unConsistTimeCards Not passed: ' . 'Should be timeConsistencyWarnings present');
        } else {
            echo 'unConsistTimeCards test passed: - OK' . PHP_EOL;
        }
    }
} catch (TravelException $e) {
    echo 'unConsistTimeCards test Not passed: ' . $e->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    print ('unConsistTimeCards Not passed: Something went wrong ' . $e->getMessage() . PHP_EOL);
}
echo $testHR;


// test $unConsistPathCards

$unConsistPathCards = 'take->Train|from->Madrid|to->Barselona|departs->2017-05-26T13:00:00+0000|arrives->2017-05-26T12:15:00+0000|board->78A|seat->45B|baggage->null;';
$unConsistPathCards .= 'take->Flight|from->Stockholm|to->New York JFK|departs->2017-05-26T20:00:00+0000|arrives->2017-05-26T23:00:00+0000|gate->22|board->SK22|seat->7B|baggage->will we automatically transferred from your last leg;';
$unConsistPathCards .= 'take->Bus|from->xsxsxs|to->Gerona Airport|departs->2017-05-26T13:00:00+0000|arrives->2017-05-24T15:00:00+0000;';
$unConsistPathCards .= 'take->Flight|from->Gerona Airport|to->Stockholm|departs->2017-05-26T16:00:00+0000|arrives->2017-05-26T18:00:00+0000|gate->45B|board->SK455|seat->3A|baggage->drop at ticket counter 344';


$api = new Travel($token);
try {
    $result = $api->call('buildResult', $unConsistPathCards);
    if ($result instanceof Calc) {
        if (!$result->pathConsistencyWarnings) {
            print ('unConsistPathCards Not passed: ' . 'Should be pathConsistencyWarnings present');
        } else {
            echo 'unConsistPathCards test passed: ' . print_r($result->pathConsistencyWarnings, true) . PHP_EOL;
        }
    }
} catch (TravelException $e) {
    echo 'unConsistPathCards test Not passed: ' . $e->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    print ('unConsistPathCards Not passed: Something went wrong ' . $e->getMessage() . PHP_EOL);
}
echo $testHR;



// test $validCards
$api = new Travel($token);

$validCards = 'take->Train|from->Madrid|to->Barselona|departs->2017-05-26T13:00:00+0000|arrives->2017-05-26T12:15:00+0000|board->78A|seat->45B|baggage->null;';
$validCards .= 'take->Flight|from->Stockholm|to->New York JFK|departs->2017-05-26T20:00:00+0000|arrives->2017-05-26T23:00:00+0000|gate->22|board->SK22|seat->7B|baggage->will we automatically transferred from your last leg;';
$validCards .= 'take->Bus|from->Barcelona|to->Gerona Airport|departs->2017-05-26T13:00:00+0000|arrives->2017-05-24T15:00:00+0000;';
$validCards .= 'take->Flight|from->Gerona Airport|to->Stockholm|departs->2017-05-26T16:00:00+0000|arrives->2017-05-26T18:00:00+0000|gate->45B|board->SK455|seat->3A|baggage->drop at ticket counter 344';

try {
    $result = $api->call('buildResult', $validCards);
    if ($result instanceof Calc) {
        print ('validCards Test passed: ' . ' - OK' . PHP_EOL);
    }
} catch (TravelException $e) {
    print ('Not passed: TravelException ' . $e->getMessage() . PHP_EOL);
} catch (\Exception $e) {
    print ('Not passed: Something went wrong ' . $e->getMessage() . PHP_EOL);
}
echo $testHR;


