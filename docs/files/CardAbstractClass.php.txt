<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:22
 */

namespace Dblanko\Travel;
use Dblanko\Travel\Exceptions\TravelException;

/**
 * Class CardAbstractClass
 * @package Dblanko\Travel
 *
 * @property int $departs
 * @property int $arrives
 * @property string $from
 * @property string $to
 */

abstract class CardAbstractClass implements CardInterface
{
    // using protected properties to avoid it changes out of class

    /** @var string $from From field of the card. */
    protected $from;

    /** @var string $to Destination field the card. */
    protected $to;

    /** @var integer $departs depart time. */
    protected $departs;

    /** @var integer $arrives arrive time. */
    protected $arrives;


    public function setFrom($from)
    {
        $this->from = $from;
    }

    public function setTo($to)
    {
        $this->to = $to;
    }

    public function setArrive($arrives)
    {
        $this->arrives = strtotime($arrives);
    }

    public function setDeparts($departs)
    {
        $this->departs = strtotime($departs);
    }

    /**
     * @param array $cardData
     * @return $this
     */
    public function setAdditionalData(array $cardData)
    {
        $ref = new \ReflectionClass(get_class($this));
        $props = array_filter($ref->getProperties(), function($property) {
            return $property->class == get_class($this);
        });


        //$props = array_keys(get_class_vars(get_class($this)));

        foreach ($props as $classProperty) {
            $propName = $classProperty->name;
            if ($propName == 'type') { //skip predefined class property
                continue;
            }

            if (!isset($cardData[$propName])) {
                $this->$propName = null;
            } else {
                $this->$propName = $cardData[$propName];
                unset($cardData[$propName]);
            }
        }

        if (count($cardData) > 0) {
            // do notice about unnecessary card data
        }

        return $this;
    }

    /**
     * magic getter for
     * @param string $property
     * @return mixed $property|false
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            // there can add allowed to access properties list
            return $this->$property;
        }
        return false;
    }

    /**
     * Disable set props availability
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        throw new \Exception("Cannot add new property \$$name to instance of " . __CLASS__);
    }
}
