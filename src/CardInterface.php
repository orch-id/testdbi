<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:19
 */

namespace Dblanko\Travel;


interface CardInterface
{
    public function setFrom($from);
    public function setTo($to);
    public function setArrive($arrives);
    public function setDeparts($departs);
}