<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:19
 */

namespace Dblanko\Travel;


use Dblanko\Travel\Exceptions\TravelException;

class Calc
{
    protected $prevDestination;
    protected $prevTimePoint;

    public $cards;
    public $pathConsistencyWarnings = [];
    public $timeConsistencyWarnings = [];
    public $timeNotice = [];


    /**
     * Calc constructor.
     * @param CardAbstractClass[] $cards
     * @throws TravelException if not correct Card object
     */
    public function __construct(array $cards)
    {
        foreach ($cards as $card) {
            if (!$card instanceof CardAbstractClass) {
                throw new TravelException("Not a instance of CardAbstractClass");
            }
        }

        $this->cards = $cards;
        $this->orderByDepartsTime();

        foreach ($this->cards as $card) {
            $this->checkPathConsistency($card);
            $this->checkTimeConsistency($card);
        }

    }

    protected function orderByDepartsTime()
    {
        usort($this->cards, [__CLASS__, 'orderByDepart']);
    }

    protected static function orderByDepart($a, $b)
    {
        if ($a->departs == $b->departs) {
            return 0;
        }
        return ($a->departs < $b->departs) ? -1 : 1;
    }

    protected function checkPathConsistency($card)
    {
            if (empty($this->prevDestination)) {
                $this->prevDestination = $card->to;
                return;
            }

            if ($this->prevDestination != $card->from) {
                $this->pathConsistencyWarnings[] = 'next departs '
                    . $card->from
                    . ' not in prev arrival '
                    . $this->prevDestination;
            }
            $this->prevDestination = $card->to;
    }

    protected function checkTimeConsistency($card)
    {
        $this->timeConsistencyWarnings[] = [];
        $this->timeNotice[] = [];

        if (empty($this->prevTimePoint)) {
            $this->prevTimePoint = time();
        }

        $timeDelta = $card->departs - $this->prevTimePoint;

        if ($timeDelta < 1) {
            $this->timeConsistencyWarnings[] = $timeDelta;
        }

        if($timeDelta < 3600) {
            $this->timeNotice[] = $timeDelta;
        }

        $this->prevTimePoint = $card->arrives;
    }
}