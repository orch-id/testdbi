<?php

/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:14
 */

namespace Dblanko\Travel;
use Dblanko\Travel\Exceptions\TravelException;

class TravelClass
{
    /** @var string API Access Token */
    private static $token = null;

    /** @var string token for instance */
    private $instanceToken;

    /** @var array added board cards */
    private $cards = [];

    /**
     * @param string|null $token API access control token
     * @throws TravelException if not set or fake
     */
    public function __construct($token = null)
    {
        if ($token === null) {
            if (self::$token === null) {
                $msg = 'Empty API Token. ';
                $msg .= 'Use Travel::setToken get new, or define it in constructor.';
                throw new TravelException($msg);
            }
        } else {
            self::validateToken($token);
            $this->instanceToken = $token;
        }
    }

    /**
     * Set instance token
     * @param $token string
     * @return void
     */
    public static function setToken($token)
    {
        self::validateToken($token);
        self::$token = $token;
    }

    /**
     * Check is allowed this token
     * @param $token
     * @return bool
     */
    private static function validateToken($token)
    {
        if (!is_string($token)) {
            throw new \InvalidArgumentException('Invalid token format');
        }
        if (strlen($token) < 6) {
            throw new \InvalidArgumentException('Token "' . $token . '" too short and not valid.');
        }
        // add more validation rules here
        return true;
    }

    /**
     * Parse input format to array
     * Use explode because I can and it simply and faster then regExp
     * @param string $card
     * @return array
     */
    public static function parseInput($card)
    {
        $data = explode('|', $card);
        $cardData = [];

        if (!is_array($data)) {
            throw new TravelException('maybe bad data format');
        }

        foreach ($data as $item) {
            $item = explode('->', $item);
            if (!is_array($item)) {
                continue;
            }

            $itemName = trim($item[0]);
            //there can rename all fields
            if ($itemName == 'take') {
                $itemName = 'type';
            }

            if ($itemName) {
                $cardData[$itemName] = trim($item[1]);
            }
        }

        return $cardData ?: null ;
    }

    public static function parseOutput($card)
    {
        $outPut = [];
        $fieldOrder = [
            'type', 'from', 'to', 'departs', 'arrives', 'gate', 'board', 'seat', 'baggage'
        ];

        foreach ($fieldOrder as $row) {
            $rowVal = '';
            $rowVal = $card->$row;

            if (isset($rowVal)) {
                if ($row == 'type') {
                    $row = 'Take'; // for User friendly can rename many fields
                }

                if ($row == 'departs' || $row == 'arrives') {
                    $rowVal = date(DATE_ATOM, $rowVal);
                }
                $outPut[] = $row .'->'. $rowVal ?: 'not assignment';
            }
        }
        $outPut = implode('|', $outPut);
        return $outPut . ';' . PHP_EOL;
    }

    /**
     * Add board cards array to travel
     * @param string $cards board cards from input list
     * @throws TravelException
     */
    protected function addCards($cards)
    {
        if (!$cards) {
            throw new TravelException('empty data');
        }

        $cards = explode(';', $cards);
        if (!is_array($cards)) {
            throw new TravelException('maybe bad data format');
        }

        foreach ($cards as $card) {
            $card = self::parseInput($card);
            if ($card) {
                $this->addCard($card);
            }
        }
    }


    /**
     * @param string $method API action Name
     * @param string $data JSON data from cards scanner
     * @return string JSON travel info
     * @throws TravelException
     */
    public function call($method, $data)
    {
        //TODO: check auth by token

        $ref = new \ReflectionClass($this);

        if (!$ref->hasMethod($method)) {
            throw new TravelException('Api method not exists');
        }

        $this->addCards($data);
        return $this->$method();
    }

    /**
     * Compose processed data to response format
     * @return Calc
     * @throws TravelException
     */
    protected function buildResult()
    {
        if (empty($this->cards)) {
            throw new TravelException('Empty cards list');
        } else {
            return new Calc($this->cards);
        }
    }


    /**
     * Produce boarding cards objects
     * @param $card
     * @return void
     */
    protected function addCard(array $cardData)
    {
        /** @var CardAbstractClass $card */
        $card = FactoryClass::create($cardData['type']);

        // There is no need to call separately, it use only for interface implementation demo.
        // On production I'll realise this logic in __constructor
        $card->setFrom($cardData['from']);
        $card->setTo($cardData['to']);
        $card->setArrive($cardData['arrives']);
        $card->setDeparts($cardData['departs']);
        $this->cards[] = $card->setAdditionalData($cardData);
    }

}