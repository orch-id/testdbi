<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:25
 */

namespace Dblanko\Travel;

/**
 * The class uses for creating a card object
 */
class FlightCard extends CardAbstractClass {

    /** @var string $gate gate info. */
    protected $gate;

    /** @var string $board board number. */
    protected $board;

    /** @var string $seat seat info. */
    protected $seat;

    /** @var string $baggage baggage info. */
    protected $baggage;

    /** @var string $type card type. */
    protected $type = 'Flight';


}