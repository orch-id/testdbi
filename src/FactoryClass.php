<?php
/**
 * Created by PhpStorm.
 * User: dmitryb
 * Date: 5/24/17
 * Time: 20:20
 */

namespace Dblanko\Travel;


abstract class FactoryClass
{
    protected static $allowedTypes = ['Flight', 'Bus', 'Train'];

    public static function create($type) {

        if (!in_array($type, self::$allowedTypes)) {
            throw new \Exception('Security error. Not allowed type ' . $type);
        }

        try {
            $className = 'Dblanko\\Travel\\' . $type . 'Card';
            return new $className();
        }
        catch (\Exception $e) {
            throw new \Exception('Card type do not defined! ' . $e->getMessage());
        }
    }
}